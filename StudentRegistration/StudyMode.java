package StudentRegistration;
import java.util.Scanner;

public class StudyMode{
  String study_mode, course_name;
  Integer semester, credit_hour;
  Double amount;
  Scanner console = new Scanner(System.in);
  
  public void inputLabel(String value){
    System.out.print("\033[0;96m -> " + "\033[1;34m" + value + "\033[0m");
  }

  public void warningLabel(String value){
    System.out.print("\033[0;93m" + value + "\033[0m");
  }

  public void errorLabel(String value){
    System.out.print("\033[0;91m" + value + "\033[0m");
  }
  
  public void normalLabel(String value){
    System.out.print("\033[0m" + value + "\033[0m");
  }
  
  public void successLabel(String value){
    System.out.print("\033[0;92m" + value + "\033[0m");
  }

  public StudyMode StudtModeMain(){
    System.out.println("");
    System.out.println("");
    studyBanner();
    StudyMode temp_studyMode = new StudyMode();
    temp_studyMode.setStudyName();
    temp_studyMode.setCourseName();
    temp_studyMode.setDuration();
    temp_studyMode.calcAmount();
    return temp_studyMode;
  }

  public void studyBanner(){
    System.out.print("\033[H\033[2J");  
    System.out.flush();
    System.out.println("====================");
    System.out.println("|    Study Detail  |");
    System.out.println("====================");
  }

  public void setStudyName (){
    warningLabel("\t1. Research");
    warningLabel("\t2. Course Work\n");
    inputLabel("Select Study Mode : ");
    this.study_mode = console.nextLine();
    if (!(this.study_mode.equals("1") || this.study_mode.equals("2"))){
      errorLabel("Wrong Option !!\n");
      setStudyName();
    }
    if (this.study_mode.equals("1") || this.study_mode.equals("2")){
      successLabel("Good Choice 😘 \n\n");
      if (this.study_mode.equals("1")){
        this.study_mode = "Research";
      }else{
        this.study_mode = "Course Work";
      }
    }
    return;
  }
  
  public String getStudyName (){
    return this.study_mode;
  }
  
  public void setCourseName (){
    warningLabel("\n\t1. Science");
    warningLabel("\t2. Engineering");
    warningLabel("\t3. Art & Social Science\n");
    inputLabel("Enter Course Number [ eg: 1 ]: ");
    this.course_name = console.nextLine();
    if (!(this.course_name.equals("1") || this.course_name.equals("2") || this.course_name.equals("3"))){
      errorLabel("Wrong Option !!");
      setCourseName();
    }
    
    if (this.course_name.equals("1") || this.course_name.equals("2") || this.course_name.equals("3")){
      successLabel("Thank you for correct 😘 \n");
      if (this.course_name.equals("1")){
        this.course_name = "Science";
      }else if (this.course_name.equals("2")){
        this.course_name = "Engineering";
      }else {
        this.course_name = "Art & Social Science";
      }
    }
    return;
  }
  
  public String getCourseName (){
    return this.course_name;
  }
  
  public void setDuration (){
    if (this.study_mode.equals("Research")){
      inputLabel("Semester : ");
      warningLabel("*enter amount of credit hour you need* ");
      inputLabel(" : ");
      this.semester = console.nextInt();
    } else {
      inputLabel("Credit Hour ");
      warningLabel("*enter amount of credit hour you need* ");
      inputLabel(" : ");
      this.credit_hour = console.nextInt();
    }
  }
  
  public Integer getDuration (){
    if (this.semester != null) {
      return this.semester;
    }else{
      return this.credit_hour;
    }
  }

  public Double calcAmount(){
    if (this.study_mode.equals("Research")){
      inputLabel("Per Semester : ");
      if(this.course_name.equals("Science")){
        this.amount = 200.00 * semester;
        normalLabel("RM 200.00\n");
        inputLabel("Total Course Fee : ");
        normalLabel("RM " + this.amount + "\n");
      }else if (this.course_name.equals("Engineering")){
        this.amount = 500.00 * semester;
        normalLabel("RM 500.00\n");
        inputLabel("Total Course Fee : ");
        normalLabel("RM " + this.amount + "\n");
      }else{
        this.amount = 300.00 * semester;
        normalLabel("RM 300.00\n");
        inputLabel("Total Course Fee : ");
        normalLabel("RM " + this.amount + "\n");
      }
    }else {
      inputLabel("Per Credit Hour : ");
      if(this.course_name.equals("Science")){
        this.amount = 100.00 * credit_hour;
        normalLabel("RM 100.00\n");
        inputLabel("Total Course Fee : ");
        normalLabel("RM " + this.amount + "\n");
      }else if (this.course_name.equals("Engineering")){
        this.amount = 300.00 * credit_hour;
        normalLabel("RM 300.00\n");
        inputLabel("Total Course Fee : ");
        normalLabel("RM " + this.amount + "\n");
      }else{
        this.amount = 150.00 * credit_hour;
        normalLabel("RM 150.00\n");
        inputLabel("Total Course Fee : ");
        normalLabel("RM " + this.amount + "\n");
      }
    }
    return this.amount;
  }
}