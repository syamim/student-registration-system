package StudentRegistration;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Report {
  Student student_detail; //Declaring Student object
  StudyMode study_mode; //Declaring Study Mode object
  Double total_amount ; //Declaring Total Amount to Pay
  Scanner console = new Scanner(System.in); //Declare library function to use

  public static void main(String[] args) {
  Scanner console = new Scanner(System.in); //Declare library function to use
  String choice = "Y";
    do{
      
      clearConsole();
      Report temp_report = new Report();
      Student color = new Student();
      //welcome message
      try {
        System.out.print(banner());
        System.out.println("");
      } catch (Exception e) {
        System.out.println("Welcome to Student Registration System");
        System.out.println("======================================");
      }

      Boolean correctInput = false;
      String option = "";
      do {
        color.inputLabel("  What you want to do today? 🤔 \n");
        color.warningLabel("  1. Register Student");
        color.normalLabel(" || ");
        color.warningLabel("  2. View Last Report");
        color.normalLabel(" || ");
        color.warningLabel("  3. View Student List");
        color.normalLabel(" || ");
        color.warningLabel("  4. Exit Program\n");
        color.inputLabel("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        color.warningLabel("Enter you choice : ");
        option = console.nextLine();
        if (option.equals("1") || option.equals("2") || option.equals("3") || option.equals("4")){
          correctInput = true;
        }else {
          clearConsole();
          color.normalLabel("==================================================\n");
          color.errorLabel("=  Wrong selection. Please select wisely 😡 😡 😡 =\n");
          color.normalLabel("==================================================\n");
        }
        
      } while (correctInput == false);
      
      if (option.equals("1")){
        //driver menu
        temp_report.setStudentDetail();
        temp_report.setStudyMode();
        temp_report.calcTotalAmount();
        
        try {
          temp_report.saveStudent();
        } catch (IOException e) {
          System.out.print("fail to save student data");
          e.printStackTrace();
        }
        
        try {
          temp_report.generateReport();
        } catch (Exception e) {
          System.out.print("fail to generate report");
          e.printStackTrace();
        }
        color.successLabel("Thank you for registering 😘😘😘");
        
        color.inputLabel("Continue ? [ Y | N] ");
        String temp_x = console.nextLine();
        if (temp_x.equals("N")){
          choice = "N";
        }
      }else if(option.equals("2")){
        clearConsole();
        try {
          temp_report.printReport();
        } catch (IOException e) {
          e.printStackTrace();
        }
        color.inputLabel("Continue ? [ Y | N] ");
        String temp_x = console.nextLine();
        if (temp_x.equals("N")){
          choice = "N";
        }
      }else if(option.equals("4")){
        choice = "N";
      }else if(option.equals("3")){
        Table list = new Table();
        list.printTable();
        color.inputLabel("Continue ? [ Y | N] ");
        String temp_x = console.nextLine();
        if (temp_x.equals("N")){
          choice = "N";
        }
      }
    }while(choice.equals("Y") || choice.equals("y"));
  }

  public static String banner() throws Exception {
    String path = System.getProperty("user.dir") + "/banner.txt";
    return new String(Files.readAllBytes(Paths.get(path)));
  }

  public void setStudentDetail() {
    Student temp_student = new Student();
    this.student_detail = temp_student.main();
  }

  public Student getStudentDetail(){
    return this.student_detail;
  }

  public void setStudyMode(){
    Student temp_student = new Student();
    this.study_mode = temp_student.StudtModeMain();
  }

  public void saveStudent() throws IOException{
    FileWriter path = new FileWriter(System.getProperty("user.dir") + "/student_list.csv",true);
    PrintWriter student_list = new PrintWriter(path);
    student_list.println(this.student_detail.name + "," + this.student_detail.phone_number + "," + this.student_detail.matric_number);
    student_list.close();
  }

  public void generateReport() throws IOException{
    PrintWriter report = new PrintWriter(System.getProperty("user.dir") + "/report.txt");
    report.println("Student Registration Report");
    report.println("");
    report.println("Student Detail");
    report.println("Name : " + this.student_detail.name);
    report.println("Phone Number : " + this.student_detail.phone_number);
    report.println("Matric Number : " + this.student_detail.matric_number);
    report.println("");
    report.println("Study Mode Detail");
    report.println("Name : " + this.study_mode.study_mode);
    report.println("Name : " + this.study_mode.course_name);
    if (this.study_mode.study_mode.equals("Research")){
      report.println("Semester : " + this.study_mode.semester);
    }else{
      report.println("Credit Hour : " + this.study_mode.credit_hour);
    }
    report.println("");
    report.println("Other Fee");
    report.println("Library Fee : RM 100.00");
    report.println("Administration Fee : RM 50.00");
    student_detail.inputLabel("Are you Malaysian?");
    student_detail.warningLabel(" ( y | n )\n");
    String nationalitu = console.nextLine();
    if (nationalitu.equals("y")){
      report.println("Malaysia Student Fee : RM 50.00");
      this.total_amount += 50.00;
    }else {
      report.println("International Student Fee : RM 100.00");
      this.total_amount += 100.00;
    }
    report.println("Total Amount : RM " + this.total_amount);
    report.println("Thank You for Registering with Us 😘😘");
    report.close();
  }

  public Double calcTotalAmount(){
    this.total_amount = this.study_mode.amount + 100.00 + 50.00;
    return this.total_amount;
  }

  public void printReport() throws IOException {
    Student color = new Student();
    BufferedReader list = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/report.txt"));
    String line = "";
  
    while ((line = list.readLine()) != null) {
      color.inputLabel(line + "\n");
    }
    list.close();
  }

  public final static void clearConsole(){
    System.out.print("\033[H\033[2J");  
    System.out.flush();
  }
}