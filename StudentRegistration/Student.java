package StudentRegistration;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Student extends StudyMode {
  String name, phone_number, matric_number; // Declaring variable
  Scanner console = new Scanner(System.in); // Declaring variable to input library

  public Student main() {
    banner(); // Section Message
    Student temp_student = new Student(); // Declaring new object
    temp_student.setName();
    temp_student.setPhoneNumber();
    temp_student.setMatricNumber();
    return temp_student; // returning student object
  }

  public void banner() {
    System.out.println("====================");
    System.out.println("|  Student Detail  |");
    System.out.println("====================");
    System.out.println("");
  }

  public void setName() {
    inputLabel("Student Name : ");
    this.name = console.nextLine();
  }

  public String getName() {
    return this.name;
  }

  public void setPhoneNumber() {
    inputLabel("Phone Number : ");
    this.phone_number = console.nextLine();
  }

  public String getPhoneNumber() {
    return this.phone_number;
  }

  public void setMatricNumber() {
    warningLabel("Generating Metric Number....\n");

    String line = "";  
    String splitBy = ",";  
    String temp_matric_number = "";
    try {  
      BufferedReader br = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/student_list.csv"));  
      while ((line = br.readLine()) != null) {  
        String[] student_data = line.split(splitBy); // use comma as separator
        temp_matric_number = student_data[2].trim();
      }
      br.close();
      this.matric_number = "00" + (Integer.parseInt(temp_matric_number) + 1);
    } catch (IOException e) {
      errorLabel("Error Parsing File\n");
      warningLabel("Matric Number set to default value\n");
      this.matric_number = "001";
      e.printStackTrace();
    }
    inputLabel("Matric Number : ");
    normalLabel(this.matric_number);
  }
  
  public String getMatricNumber (){
    return this.matric_number;
  }
}
